# CAMPtalk

An R package of tools that aid with querying the CTC's CAMP data base.

To install in R:
    
```
install.packages("remotes") 
remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/r-packages/CAMPtalk")
```

Load `CAMPtalk` into memory:
```
require(CAMPtalk)
```

The function `writeScript` will make it easier to start things, it builds example scripts for tools in this package. The help file has an example:
```
?CAMPtalk::writeScript()
```

In the example we can see how to seek what demo scripts are available:
```
demo(package = "CAMPtalk")
```

To save and open a specific demo script (this one named "aa_demo"):
```
writeScript("aa_demo", package="CAMPtalk")

```
